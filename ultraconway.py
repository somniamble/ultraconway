#! /usr/bin/python3

from random import randint
from itertools import product
from time import sleep

#BOUNDARY_DEFAULT = lambda: randint(0,1)
BOUNDARY_DEFAULT = lambda: 0
WIDTH=100
HEIGHT=50
SIZE=100

LONELY=lambda: randint(1,2)
CROWD=lambda: randint(4,5)
#RESURRECT=lambda: randint(2,3)
RESURRECT=lambda: randint(2,3)


# this configuration also goes forever
# LONELY=lambda: randint(1,2)
# CROWD=lambda: randint(4,6)
# #RESURRECT=lambda: randint(2,3)
# RESURRECT=lambda: randint(1,4)

ALIVE="·" # "•" # "."  # "·"
DEAD=" "

def print_grid(_grid):
  for line in _grid:
    for cell in line:
      print(ALIVE if cell is 1 else DEAD, end=" ")
    print()
  print("= " * SIZE)

def at_pos(x, y, _grid):
  if (x < 0 or y < 0 or x >= SIZE or y >= SIZE):
    return BOUNDARY_DEFAULT()
  else:
    return _grid[y][x]

def neighborlogic(x, y, _grid):
  def makeposer(g): 
    return lambda x, y: at_pos(x, y, g)
  poser = makeposer(_grid)
  # sum = poser(x-2, y-2) + poser(x, y-1) + poser(x+2, y-2) + \
  #         poser(x-1, y) + poser(x+1, y) + \
  #         poser(x-2, y+2) + poser(x, y+1) + poser(x+2, y+2)
  sum = poser(x-1, y-1) + poser(x, y-1) + poser(x+1, y-1) + \
          poser(x-1, y) + poser(x+1, y) + \
          poser(x-1, y+1) + poser(x, y+1) + poser(x+1, y+1)
  if poser(x, y) is 0:
    return 1 if sum == RESURRECT() else 0
  else:
    return 1 if (sum > LONELY() and sum < CROWD()) else 0

def step(_grid=None):
  new_grid = [[None] * SIZE for _ in range(SIZE)]
  for x, y in product(range(SIZE), repeat=2):
    new_grid[y][x] = neighborlogic(x, y, _grid)
  return new_grid

grid = [[randint(0, 1) for j in range(SIZE)] for i in range(SIZE)]
while True:
  print_grid(grid)
  grid = step(grid) 
  sleep(.05)
